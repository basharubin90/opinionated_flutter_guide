# opinionatedflutterguide

A Flutter application showcasing an opinionated starting point with most relevant architecture already set up.

## Getting Started

This project is a starting point for a Flutter application.

### The key parts to look at are:
 
* lib (directory)
* lang (directory)
* localizations_key_generator (just a simple python file that runs a script to read language json files and generated keys for typo protection to use constants)

### To make the project runnable , just run following commands: 

* flutter pub get
* flutter pub run build_runner watch --delete-conflicting-outputs
* after that just run the python script described above

### Packages used
* https://pub.dev/packages/retrofit
* https://pub.dev/packages/json_serializable
* https://pub.dev/packages/freezed
* https://pub.dev/packages/bloc
* https://pub.dev/packages/flutter_bloc
* https://pub.dev/packages/injectable
* https://pub.dev/packages/auto_route