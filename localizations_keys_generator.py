# GENERATES Strings located in this file
# lib/localization/localization_keys.dart
# whenever adding new strings, to update the keys file, just run this command like so:
# python localizations_keys_generator.py

import json

with open('lang/en.json', "r") as json_file:
    data = json.load(json_file)
    list = []
    for p in data:
        list.append('  static const ' + p + ' = "' + p + '";')

    concatRes = "class StringKeys {"
    for s in list:
        concatRes = concatRes + '\n' + s
    concatRes = concatRes + '\n' + '}'

    print(concatRes)
with open('lib/localization/localization_keys.dart', "w") as f:
    f.write(concatRes)