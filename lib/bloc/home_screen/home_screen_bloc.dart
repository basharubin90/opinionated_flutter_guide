import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hpd/utils/injection/injection.dart';
import 'package:hpd/data/network/placeholder/placeholder_repository.dart';
import 'home_screen_bloc_dto.dart';

class HomeScreenBloc extends Bloc<HomeScreenEvent, HomeScreenState> {
  @override
  HomeScreenState get initialState => HomeScreenState(counter: 0);

  @override
  Stream<HomeScreenState> mapEventToState(HomeScreenEvent event) async* {
    switch (event.type) {
      case HomeScreenEventEventType.decrement:
        // calling actual network api just to showcase
        getIt<PlaceholderRepository>().getPlaceholder(3);
        yield HomeScreenState(counter: state.counter - 1);
        break;
      case HomeScreenEventEventType.increment:
        // Simulating Network Latency
        await Future<void>.delayed(Duration(milliseconds: 500));
        yield HomeScreenState(counter: state.counter + 1);
        break;
    }
  }
}