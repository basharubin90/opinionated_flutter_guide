import 'package:freezed_annotation/freezed_annotation.dart';

part 'home_screen_bloc_dto.freezed.dart';

@freezed
abstract class HomeScreenState with _$HomeScreenState {
  const factory HomeScreenState({
    int counter,
  }) = _HomeScreenState;
}

@freezed
abstract class HomeScreenEvent with _$HomeScreenEvent {
  const factory HomeScreenEvent({
    HomeScreenEventEventType type,
  }) = _HomeScreenEvent;
}

enum HomeScreenEventEventType {
  increment,
  decrement
}