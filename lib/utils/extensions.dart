import 'package:async/async.dart';
import 'package:auto_route/auto_route.dart';
import 'package:dio/dio.dart';
import 'package:flutter/widgets.dart';
import 'package:hpd/data/model/generic/generic_response.dart';
import 'package:hpd/localization/app_localizations.dart';
import 'package:hpd/utils/toplevel.dart';
import 'package:hpd/utils/utilityclasses.dart';


extension KotlinUtils<T extends Object, R extends Object> on T {
  T also(void block(T it)) {
    block?.call(this);
    return this;
  }

  R let(R block(T it)) {
    return block?.call(this);
  }

  T takeIf(bool predicate(T it)) {
    return predicate?.call(this) == true ? this : null;
  }

  T takeUnless(bool predicate(T it)) {
    return predicate?.call(this) == false ? this : null;
  }
}

extension NumberUtils on num {
  num hoursToMillis() => this * 3600000;

  num secToMillis() => this * 1000;
}

extension BuildContextUtils on BuildContext {

  String getString(String key) {
    return AppLocalizations.of(this).translate(key);
  }

  /// use strings like this Routes.secondScreen
  void navigate(String route) {
    ExtendedNavigator.of(this).pushNamed(route);
  }
}

typedef VoidOneParamFunction<T extends Object> = void Function(T it);

extension FutureUtils on Future {
  Future<GenericResponse<T>> mapToGenericResponse<T>() async {
    try {
      final response = await this;
      return Future.value(
          GenericResponse.success(DioSuccessDto(data: response)));
    } on DioError catch (e) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.
      if (e.response != null) {
        return Future.value(GenericResponse.error(DioErrorDto(
          statusCode: e.response.statusCode,
          message: e.message,
          responseData: e.response.data,
          responseHeaders: e.response.headers,
          requestOptions: e.response.request,
        )));
      } else {
        // Something happened in setting up or sending the request that triggered an Error
        return Future.value(GenericResponse.error(DioErrorDto(
          statusCode: -1,
          message: e.message,
          requestOptions: e.request,
        )));
      }
    } catch (any) {
      return Future.value(GenericResponse.error(DioErrorDto(statusCode: -1)));
    }
  }
}

extension CancelableOperationExtensions<T> on CancelableOperation<T> {
  CancelableOperation<T> emitIfNeeded(LiveData<T> liveData,
      {Function(Object, StackTrace) onError, Function() onCancel}) {
    return this.also((it) {
      it.then((data) => liveData?.emit(data),
          onError: (e, s) => onError != null
              ? onError.call(e, s)
              : logE("", error: e, stackTrace: s),
          onCancel: () =>
              onCancel != null ? onCancel.call() : logE("Operation Canceled"));
    });
  }
}
