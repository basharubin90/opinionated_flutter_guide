import 'package:auto_route/auto_route.dart';
import 'package:flutter/foundation.dart';
import 'package:hpd/routes/router.gr.dart';
import 'package:hpd/utils/extensions.dart';
import 'package:logger/logger.dart';

void repeat(int times, Function(int) action) {
  for (var index = 0; index < times; index++) {
    action?.call(index);
  }
}

ExtendedNavigatorState navigate() {
  return ExtendedNavigator.ofRouter<Router>();
}

/// Try running a function
/// also if desired get the return value or null if execution was not possible
/// this function does not oblige the inclusion of a return statement
T tryCatchAll<T extends Object>(T Function() block) {
  try {
    return block.call();
  } catch (e, s) {
    print('Something really unknown: $e with StackTrace $s');
    return null;
  }
}

// region LOGGING
final logger = Logger(
  printer: PrettyPrinter(colors: true, errorMethodCount: 0),
);

void logE(String message, {Object error, StackTrace stackTrace}) {
  if (kReleaseMode) {
    // nothing
  } else {
    message?.also((it) => logger.e("logging " + it));
    error?.also((it) => logger.e("logging " + it.toString()));
    stackTrace?.also((it) => logger.e("logging " + it.toString()));
  }
}

void logD(String message) {
  if (kReleaseMode) {
    // nothing
  } else {
    message?.also((it) => logger.d("logging " + it));
  }
}
// endregion LOGGING