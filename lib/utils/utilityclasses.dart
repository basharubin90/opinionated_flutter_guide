import 'dart:async';

import 'package:async/async.dart';

/// Used as a wrapper for data that is exposed via a sticky event holders
class Event<T extends Object> {
  T content;
  Object metadata;
  bool _consumed = false;

  Event(this.content, {this.metadata});

  /// Returns the content and prevents its use again.
  T getContentIfNotConsumed() {
    if (_consumed) {
      return null;
    } else {
      _consumed = true;
      return content;
    }
  }

  /// Returns the content, even if it's already been handled.
  T peekContent() => content;

  void invalidate() {
    _consumed = true;
  }
}

class NullWrapper<T extends Object> {
  T content;
  Object metadata;

  NullWrapper(this.content, {this.metadata});
}

/// class that makes it possible to debounce messages when they come in rapid succession
/// As Type T when no value is passed just specify Debounce<Null> as type.
/// That would be useful in cases when we just want to execute a block of code with debounce effect
/// and don't care for a specific value and just call trigger()
class Debounce<T> {
  Function(T) _block;
  RestartableTimer timer;
  T _value;
  int counter = 0;

  Debounce(Duration duration, this._block) {
    timer = RestartableTimer(duration, () {
      _block.call(_value);
    });
    timer.cancel();
  }

  void trigger({T value, bool forceImmediateExecution = false}) {
    counter++;
    if (forceImmediateExecution) {
      _block.call(value);
    } else {
      _value = value;
      timer.reset();
    }
  }

  void cancel() {
    timer.cancel();
  }
}

/// class that makes it possible to debounce messages when they come in rapid succession
/// As Type T when no value is passed just specify Debounce<Null> as type.
/// That would be useful in cases when we just want to execute a block of code with debounce effect
/// and don't care for a specific value and just call trigger()
class Throttle<T> {
  Function(T) _block;
  int lastTimeInteracted = 0;
  int counter = 0;
  Duration duration;

  Throttle(this.duration, this._block);

  void trigger({T value, bool forceImmediateExecution = false}) {
    counter++;
    var now = _getTimeNow();
    if (forceImmediateExecution ||
        now - lastTimeInteracted > duration.inMilliseconds) {
      lastTimeInteracted = now;
      _block.call(value);
    }
  }

  int _getTimeNow() => DateTime.now().millisecondsSinceEpoch;
}

class LiveData<T> {
  T current;
  final _controller = StreamController<T>();

  Stream<T> get stream => _controller.stream.asBroadcastStream();

  void emit(T event) {
    current = event;
    _controller.add(event);
  }

  void close() {
    _controller.close();
  }
}
