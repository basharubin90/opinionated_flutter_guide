import 'dart:async';

import 'package:hpd/utils/utilityclasses.dart';

/// sample singleton that might be used as a Manager class that can also distribute data as an Event Bus
class WebsocketManager {
  static final WebsocketManager _instance = WebsocketManager._internal();

  factory WebsocketManager() => _instance;

  WebsocketManager._internal() {
    // init things inside this
  }

  final events = LiveData<int>();

  void testInputIntoStream() {
    Stream<int>.value(2).listen((event) {
      events.emit(event);
    });
  }
}
