import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hpd/bloc/home_screen/home_screen_bloc.dart';
import 'package:hpd/bloc/home_screen/home_screen_bloc_dto.dart';
import 'package:hpd/localization/localization_keys.dart';
import 'package:hpd/routes/router.gr.dart';
import 'package:hpd/utils/extensions.dart';
import 'package:hpd/utils/toplevel.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
              "HPD"
          ),
        ),
        body: Center(
          child: BlocProvider(
            create: (_) => HomeScreenBloc(),
            child: BlocConsumer<HomeScreenBloc, HomeScreenState>(
              /// here is the right place to recreate the view
              builder: (context, state) {
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      context.getString(StringKeys.my_name),
                    ),
                    Text(
                      'HPD 👍: ${state.counter}',
                      style: Theme.of(context).textTheme.headline4,
                    ),
                    RaisedButton(
                      onPressed: () => context.bloc<HomeScreenBloc>().add(HomeScreenEvent(type: HomeScreenEventEventType.increment)),
                      child: Text(context.getString(StringKeys.increment)),
                    ),
                    RaisedButton(
                      onPressed: () => context.bloc<HomeScreenBloc>().add(HomeScreenEvent(type: HomeScreenEventEventType.decrement)),
                      child: Text(context.getString(StringKeys.decrement)),
                    ),
                  ],
                );
              },
              /// listener is used to fire side effects like logging and Analytics
              /// builder is not the right place for that as it might be called even in case of no new state in case UI tree decides to rebuild
              listener: (BuildContext context, HomeScreenState state) => logE("current state is ${state.counter}"),
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            context.navigate(Routes.secondScreen);
          },
          tooltip: 'navigate',
          child: Icon(Icons.send),
        ),
    );
  }
}
