import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hpd/localization/localization_keys.dart';
import 'package:hpd/utils/extensions.dart';

class SecondScreen extends StatelessWidget {
  const SecondScreen();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("HPD"),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                context.getString(StringKeys.welcome),
                style: Theme.of(context).textTheme.headline4,
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
    );
  }
}