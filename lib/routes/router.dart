import 'package:auto_route/auto_route_annotations.dart';
import 'package:hpd/screens/home_screen.dart';
import 'package:hpd/screens/second_screen.dart';

@CupertinoAutoRouter(generateNavigationHelperExtension: true)
class $Router {
  @initial
  HomeScreen initialPage;
  SecondScreen secondScreen;
}