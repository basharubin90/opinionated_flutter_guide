import 'package:dio/dio.dart';
import 'package:hpd/data/model/generic/generic_response.dart';
import 'package:hpd/data/model/placeholder/placeholder_model.dart';
import 'package:hpd/utils/extensions.dart';
import 'package:injectable/injectable.dart';
import 'package:retrofit/retrofit.dart';

import '../ApiClient.dart';

part 'placeholder_service.g.dart';

@RestApi()
abstract class PlaceholderEndpoints {
  factory PlaceholderEndpoints(Dio dio, {String baseUrl}) =
  _PlaceholderEndpoints;

  @GET("/posts/{id}")
  Future<PlaceholderModel> getPlaceholder(@Path("id") int id);
}

@injectable
class PlaceholderService {
  PlaceholderEndpoints _endpoints;

  PlaceholderService() {
    _endpoints = PlaceholderEndpoints(ApiClient.client);
  }

  Future<GenericResponse<PlaceholderModel>> getPlaceholder(int id) =>
      _endpoints.getPlaceholder(id).mapToGenericResponse();
}
