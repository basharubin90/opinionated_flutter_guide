import 'package:hpd/data/model/generic/generic_response.dart';
import 'package:injectable/injectable.dart';
import 'package:hpd/data/model/placeholder/placeholder_model.dart';
import 'placeholder_service.dart';

/// Repository is the entry point to the data layer
/// Repository is where data flow is directed/mutated towards and from network or DB or Cache
@injectable
class PlaceholderRepository {
  PlaceholderService _service;

  PlaceholderRepository(this._service);

  Future<GenericResponse<PlaceholderModel>> getPlaceholder(int id) {
    return _service.getPlaceholder(id);
  }
}