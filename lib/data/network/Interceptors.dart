import 'package:dio/dio.dart';
import 'package:hpd/utils/toplevel.dart';

class RequestHeadersInterceptor extends InterceptorsWrapper {
  @override
  Future onRequest(RequestOptions options) {
    logD("REQUEST[${options?.method}] => PATH: ${options?.path}");
    return super.onRequest(options);
  }

  @override
  Future onResponse(Response response) {
    logD(
        "RESPONSE[${response?.statusCode}] => PATH: ${response?.request?.path}");
    return super.onResponse(response);
  }

  @override
  Future onError(DioError err) {
    logD("ERROR[${err?.response?.statusCode}] => PATH: ${err?.request?.path}");
    return super.onError(err);
  }
}
