import 'package:dio/dio.dart';
import 'package:hpd/data/network/Interceptors.dart';
import 'package:hpd/utils/extensions.dart';
import 'package:injectable/injectable.dart';

@injectable
class ApiClient {
  static RequestHeadersInterceptor requestHeadersInterceptor =
      RequestHeadersInterceptor();

  static Dio client =
      new Dio(BaseOptions(baseUrl: "https://jsonplaceholder.typicode.com"))
          .also((it) {
    it.interceptors.add(requestHeadersInterceptor);
  });
}
