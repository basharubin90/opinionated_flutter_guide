import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'placeholder_model.freezed.dart';

part 'placeholder_model.g.dart';

@freezed
abstract class PlaceholderModel with _$PlaceholderModel {
  const factory PlaceholderModel({
    int userId,
    int id,
    String title,
    String body
  }) = _PlaceholderModel;

  factory PlaceholderModel.fromJson(Map<String, dynamic> json) =>
      _$PlaceholderModelFromJson(json);
}