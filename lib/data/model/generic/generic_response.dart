import 'package:dio/dio.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'generic_response.freezed.dart';

/// used like this
/// Union.data(1).when(data: (value){}, loading: () {}, error: (msg) {});
@freezed
abstract class GenericResponse<T> with _$GenericResponse<T> {
  const factory GenericResponse.success([DioSuccessDto<T> value]) = Success<T>;

  const factory GenericResponse.error([DioErrorDto dioErrorDto]) = Error<T>;
}

@freezed
abstract class DioSuccessDto<T> with _$DioSuccessDto<T> {
  const factory DioSuccessDto({T data}) = _DioSuccessDto<T>;
}

@freezed
abstract class DioErrorDto with _$DioErrorDto {
  const factory DioErrorDto({
    int statusCode,
    String message,
    dynamic responseData,
    Headers responseHeaders,
    RequestOptions requestOptions,
  }) = _DioErrorDto;
}
